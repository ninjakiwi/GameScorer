package com.ninjapenguin.gamescorer.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel on 30/07/17.
 */

public class Player {
    public String name;
    public List<Integer> scores;

    public Player(String name) {
        this.name = name.substring(0, 1).toUpperCase() + name.substring(1);
        this.scores = new ArrayList<>();
    }
}
