package com.ninjapenguin.gamescorer;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ninjapenguin.gamescorer.data.Player;

import java.util.Arrays;
import java.util.List;

/**
 * Created by daniel on 30/07/17.
 */

public class ScoresFragment extends Fragment {
    Player bob = new Player("bobolassadsaeds");
    private int mNum;
    private List<Player> players = Arrays.asList(
            bob,
            new Player("bill"),
            new Player("dan"),
            new Player("dafdskjdslkjlkjdsan"),
            new Player("dan"),
            new Player("dan"),
            new Player("dan"),
            new Player("dan"),
            new Player("dan"),
            new Player("dasddsadsadsan"),
            new Player("dan"),
            new Player("dan"),
            new Player("asddsaasdsadasddsa")
    );

    {
        bob.scores.add(20);
        bob.scores.add(-4);
        bob.scores.add(5);
    }

    public static ScoresFragment newInstance(int num) {
        ScoresFragment f = new ScoresFragment();

        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
    }

    /**
     * When creating, retrieve this instance's number from its arguments.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments() != null ? getArguments().getInt("num") : 1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_scores, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        RecyclerView recyclerView = getActivity().findViewById(R.id.recyclerView);
        recyclerView.setAdapter(new MyAdapter<>(players));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
    }

    private class MyAdapter<T> extends RecyclerView.Adapter<MyBindingHolder> {
        private List<T> mItems;

        MyAdapter(List<T> items) {
            mItems = items;
        }

        @Override
        public MyBindingHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.listitem_scores, viewGroup, false);
            return new MyBindingHolder(viewDataBinding);
        }

        @Override
        public void onBindViewHolder(MyBindingHolder holder, int position) {
            final T item = mItems.get(position);
            holder.getBinding().setVariable(com.ninjapenguin.gamescorer.BR.item, item);
            holder.getBinding().executePendingBindings();
        }

        @Override
        public int getItemCount() {
            return players.size();
        }
    }

    private class MyBindingHolder extends RecyclerView.ViewHolder {

        private ViewDataBinding mBinding;

        MyBindingHolder(ViewDataBinding viewDataBinding) {
            super(viewDataBinding.getRoot());
            mBinding = viewDataBinding;
        }

        ViewDataBinding getBinding() {
            return mBinding;
        }
    }
}
